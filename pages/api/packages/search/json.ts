// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import mysql from "mariadb"

type Data = {
  version: number
  limit: number
  valid: boolean
  results: any
  num_pages: any
  page: any
}


async function validator(validatee: any, value: any, dbconnection?: any) {
  if (value === undefined) {
    return true;
  }
  switch (validatee) {
    case "repo":
      let repos = ["Community-Testing", "Community", "Core", "Extra", "Multilib-Testing", "Multilib", "Testing"];
      if (repos.includes(value)) { }
      break;
    case "arch":
      let arches = ["any", "x86_64"];
      if (Array.isArray(value)) {
        value.forEach(element => {
          if (!arches.includes(element)) { return false; }
        });
      } else {
        if (!arches.includes(value)) { return false; }
      }
      break;
    case "maintainer":
      let maintainersquery = await dbconnection.execute(`SELECT * FROM maintainers`);
      maintainersquery = JSON.parse(JSON.stringify(maintainersquery));
      let maintainers: any[] = [];
      maintainersquery.forEach((element: any) => {
        maintainers.push(element.maintainers);
      });
      if (!maintainers.includes(value)) { return false; }
      break;
    case "packager":
      let [packagersquery] = await dbconnection.execute(`SELECT * FROM packagers`);
      packagersquery = JSON.parse(JSON.stringify(packagersquery));
      let packagers: any[] = [];
      packagersquery.forEach((element: any) => {
        packagers.push(element.packagers);
      });
      if (!packagers.includes(value)) { return false; }
      break;
    case "flagged":
      var flags = ["Flagged", "Not+Flagged", "Not Flagged"];
      if (!flags.includes(value)) { return false; }
      break;
  }
  return true;
}


export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  res.setHeader('Cache-Control', 'no-cache')
  res.setHeader('Content-Type', 'application/json')
  if (req.method === 'GET') {
    const query = req.query;

    let { page, limit } = query;

    if (Array.isArray(limit) && (Number.isInteger(Number(limit[limit.length - 1]))) && (Number(limit[limit.length - 1]) <= 250)) {
      limit = limit[limit.length - 1];
    } else {
      if (!(Number.isInteger(Number(limit)) || (Number(limit) > 250))) {
        limit = "250";
      }
    }

    if (Array.isArray(page)) {
      page = page[page.length - 1];
    } 
    
    if (page === undefined) {
      page = "1"
    }

    try {
      let { q, name, desc, repo, arch, maintainer, packager, flagged } = query;
      if (page === undefined && limit === undefined && q === undefined && name === undefined && desc === undefined && repo === undefined && arch === undefined && maintainer === undefined && packager === undefined && flagged === undefined) {
        throw "All undefined";
      }

      const dbconnection = await mysql.createConnection({
        host: process.env.DB_HOST,
        database: process.env.DB,
        user: process.env.DB_USER,
        password: process.env.DB_PASS
      });

      let validArgs: { [s: string]: Array<any> } = {
        q: Array(q!, "like"),
        pkgname: Array(name!, "strict"),
        pkgdesc: Array(desc!, "like"),
        repo: Array(repo!, "array"),
        arch: Array(arch!, "array"),
        maintainer: Array(maintainer!, "strict"),
        packager: Array(packager!, "strict"),
        flagged: Array(flagged!, "strict")
      };

      const querytostr = (name: any) => {
        if (Array.isArray(validArgs[name][0]) && (validArgs[name][0] !== undefined)) {
          validArgs[name][0] = validArgs[name][0][validArgs[name][0].length - 1];
        }
      }

      querytostr("q");
      querytostr("pkgname");
      querytostr("pkgdesc");
      querytostr("maintainer");
      querytostr("packager");
      querytostr("flagged");

      if (!(await validator('q', validArgs['q'][0]))) { throw "Invalid Arg" }
      if (!(await validator('pkgname', validArgs['pkgname'][0]))) { throw "Invalid Arg" }
      if (!(await validator('pkgdesc', validArgs['pkgdesc'][0]))) { throw "Invalid Arg" }
      if (!(await validator('repo', validArgs['repo'][0]))) { throw "Invalid Arg" }
      if (!(await validator('arch', validArgs['arch'][0]))) { throw "Invalid Arg" }
      if (!(await validator('maintainer', validArgs['maintainer'][0], dbconnection))) { throw "Invalid Arg" }
      if (!(await validator('packager', validArgs['packager'][0], dbconnection))) { throw "Invalid Arg" }
      if (!(await validator('flagged', validArgs['flagged'][0]))) { throw "Invalid Arg" }

      let wherewhat = "WHERE ";
      let wherewhatarray = Array()
      for (let [key, [value, strict]] of Object.entries(validArgs)) {
        if (value === undefined) {
          continue;
        }
        
        let operator = "";

        if (strict == 'like') {
          operator = 'LIKE';
        } else if (strict == 'strict') {
          operator = '=';
        } else if (strict == 'array') {
          operator = 'IN';
        }

        if (Array.isArray(value)) {
          let commas = "";
          for (let i = 0; i < (value.length - 1); i++) {
            if (commas == "") {commas += "?"} else {commas += ",?"};
            wherewhatarray.push(`'${value[i]}'`)
          }
          if (wherewhat == "WHERE ") {
            wherewhat += `${key} ${operator} (${commas}) `;
          } else {
            wherewhat += `AND ${key} ${operator} (${commas}) `;
          }
        } else {
          if (strict == 'like') {
            value = `%${value}%`;
          }

          if (wherewhat == "WHERE ") {
            wherewhat += `${key} ${operator} ? `;
            wherewhatarray.push(value)
          } else {
            wherewhat += `AND ${key} ${operator} ? `;
            wherewhatarray.push(value)
          }
        }
      };

      if (wherewhat == "WHERE ") {
        wherewhat = "";
      }

      const lengthquery = "SELECT count(*) OVER() AS length FROM json " +  wherewhat + "LIMIT 1"
      const [length] = await dbconnection.query(lengthquery, wherewhatarray);
      let num_pages;

      try {
        num_pages = (Math.ceil(Number(length.length) / Number(limit)));
        if (Number(page) > num_pages) {
          page = String(num_pages);
        }  
      } catch (error) {
        num_pages = 1;
      }
      const offset = (Number(page) * Number(limit)) - Number(limit);
      const dbquery = 'SELECT rawjson FROM json ' + wherewhat + ' LIMIT ' + limit + ' OFFSET ' + offset;
      const data = await dbconnection.query(dbquery, wherewhatarray);

      const obj = JSON.parse(JSON.stringify(data));
      let json: any = [];

      obj.forEach((element: any) => {
        json.push(JSON.parse(element.rawjson));
      });

      res.status(200).json({ "version": 2, "limit": Number(limit), "valid": true, "results": json, "num_pages": num_pages, "page": Number(page) });
    } catch (error: any) {
      res.status(200).json({ "version": 2, "limit": Number(limit), "valid": false, "results": [], "num_pages": undefined, "page": undefined });
    }
  }
}